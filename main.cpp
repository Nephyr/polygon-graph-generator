#include <iostream>
#include "SitePolygonGraph.h"

int main()
{
    SitePolygonGraph p;

    // TEST 1
    // p.add_node(0, 0.5);
    // p.add_node(2.5, 0.5);
    // p.add_node(3.5, 1.5);
    // p.add_node(4, 4);

    // p.add_node(3.5, 5);
    // p.add_node(2.5, 5);
    // p.add_node(2, 3.5);
    // p.add_node(1.5, 3);

    // p.add_node(1, 2.5);
    // p.add_node(0, 3);
    // p.add_node(-1, 2);
    // p.add_node(-1, 1.25);

    // TEST 2
    p.add_node(95.6 , 248);
    p.add_node(113, 343.7);
    p.add_node(201.3, 400);
    p.add_node(418, 396.5);

    p.add_node(503 , 297.4);
    p.add_node(547 , 148.7);
    p.add_node(559.4, 44);
    p.add_node(478.5 , 22.3);

    p.add_node(412.6 , 62.5);
    p.add_node(364 , 191);
    p.add_node(273.2 , 210.5);
    p.add_node(172.5 , 174.7);

    // SQUARE !!!
    // p.add_node(1,1);
    // p.add_node(3,1);
    // p.add_node(3,3);
    // p.add_node(1,3);

    // Create arches from points
    p.finalize_polygon();

    SitePolygonGraphNode a = p.randomize_poi();
    SitePolygonGraphNode b = p.randomize_poi();
    SitePolygonGraphNode c = p.randomize_poi();
    SitePolygonGraphNode d = p.randomize_poi();
    SitePolygonGraphNode e = p.randomize_poi();
    SitePolygonGraphNode f = p.randomize_poi();

    std::cout << std::endl;
    std::cout << " >> SegmentX:  " << a.getX() << std::endl
              << " >> SegmentY:  " << a.getY() << std::endl
              << "-------------------------------------------------------------------" << std::endl;
    std::cout << " >> SegmentX:  " << b.getX() << std::endl
              << " >> SegmentY:  " << b.getY() << std::endl
              << "-------------------------------------------------------------------" << std::endl;
    std::cout << " >> SegmentX:  " << c.getX() << std::endl
              << " >> SegmentY:  " << c.getY() << std::endl
              << "-------------------------------------------------------------------" << std::endl;
    std::cout << " >> SegmentX:  " << d.getX() << std::endl
              << " >> SegmentY:  " << d.getY() << std::endl
              << "-------------------------------------------------------------------" << std::endl;
    std::cout << " >> SegmentX:  " << e.getX() << std::endl
              << " >> SegmentY:  " << e.getY() << std::endl
              << "-------------------------------------------------------------------" << std::endl;
    std::cout << " >> SegmentX:  " << f.getX() << std::endl
              << " >> SegmentY:  " << f.getY() << std::endl << std::endl;

    return 1;
}
